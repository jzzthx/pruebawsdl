using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading.Tasks;

namespace Prueba
{
    class Program
    {
        static async Task Main(string[] args)
        {
            

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => { return true; };
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://wugateway2pi.westernunion.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new
                  MediaTypeWithQualityHeaderValue("application/json"));
                Task<string> response = client.GetStringAsync("https://wugateway2pi.westernunion.net/");
                response.Wait();
                /*
                if (response.Exception != null)
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<UserData>(response.Result);*/
            }
            
            //var EndpointAddress = new EndpointAddress("https://wugateway2pi.westernunion.net/");

            ServiceReference1.DASInquiryPortType dsDasSe = new ServiceReference1.DASInquiryPortTypeClient(0);



            ServiceReference1.DAS_Service_Input reqDASSe = new ServiceReference1.DAS_Service_Input();
            //reqDASSe.h2hdasrequest.channel.type = ServiceReference1.channel_type.H2H;
            ServiceReference1.h2hdasrequest h2Hdas = new ServiceReference1.h2hdasrequest();
            ServiceReference1.channel cha = new ServiceReference1.channel();
            h2Hdas.channel = cha;
            reqDASSe.h2hdasrequest = h2Hdas;
            //reqDASSe.h2hdasrequest.channel.name = "ESP";
            cha.version = "9500";
            cha.name = "ESP";
            cha.type = ServiceReference1.channel_type.H2H;
            //
            ServiceReference1.foreign_remote_system fosy = new ServiceReference1.foreign_remote_system();
            h2Hdas.foreign_remote_system = fosy;
            fosy.identifier = "WGHHPE0490T";
            fosy.reference_no = "123456665432";
            fosy.counter_id = "PE049PPA3DN";
            //  
            h2Hdas.name = "GetCountriesCurrencies";
           ServiceReference1.filters_type fity = new ServiceReference1.filters_type();
            h2Hdas.filters = fity;
            fity.queryfilter1 = "es";
            fity.queryfilter2 = "PE";
            ServicePointManager
    .ServerCertificateValidationCallback +=
    (sender, cert, chain, sslPolicyErrors) => true;
            //System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
           /* ServicePointManager.ServerCertificateValidationCallback = delegate (
           Object obj, X509Certificate certificate, X509Chain chain,
           SslPolicyErrors errors)
            {
                return (true);
            };*/
            //no se puede :(
            //validar.

            /*  var pr = dsDasSe.DAS_Service(reqDASSe);
             if (pr.h2hdasreply!=null)
             {
                Console.WriteLine(pr);
              }

              System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };*/


            //Task < ServiceReference1.DAS_Service_Output > dt = dsDasSe.DAS_ServiceAsync(reqDASSe);
            var dt = dsDasSe.DAS_ServiceAsync(reqDASSe);
            dt.Wait();
            /*if (dt.Result != null)
            {
                Console.WriteLine("--->" + dt);

            }*/
            Console.WriteLine("--->" + dt);
        }
    }
}
